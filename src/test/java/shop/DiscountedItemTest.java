package shop;

import cz.cvut.eshop.shop.DiscountedItem;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Ignore;
import org.junit.Test;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class DiscountedItemTest {
    @Test
    public void setDiscountFrom_correctDateFormat() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");
        String newDateString = "15.04.2005";

        discountedItem.setDiscountFrom(newDateString);
        Date date = discountedItem.getDiscountFrom();

        assertEquals(15, date.getDate());
        assertEquals(3, date.getMonth());
        assertEquals(105, date.getYear());
    }

    @Test
    public void setDiscountFrom_wrongDateFormat() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");
        String newDateString = "abc";

        discountedItem.setDiscountFrom(newDateString);
        Date date = discountedItem.getDiscountFrom();

        assertEquals(10, date.getDate());
        assertEquals(4, date.getMonth());
        assertEquals(117, date.getYear());
    }

    @Test
    public void testSetDiscountToCorrectDateFormatString() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");
        String newDateString = "15.04.2020";

        discountedItem.setDiscountTo(newDateString);
        Date date = discountedItem.getDiscountTo();

        assertEquals(15, date.getDate());
        assertEquals(3, date.getMonth());
        assertEquals(120, date.getYear());
    }

    @Test
    public void testSetDiscountToWrongDateFormatString() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");
        String newDateString = "abcdefs";

        discountedItem.setDiscountTo(newDateString);
        Date date = discountedItem.getDiscountTo();

        assertEquals(10, date.getDate());
        assertEquals(4, date.getMonth());
        assertEquals(118, date.getYear());
    }

    //TODO: see Ignore
    @Ignore("function getDiscountedPrice() in class DiscountedItem does not divide percents by 100" +
            " so the price result of multiplication is incorrect. " +
            "Possible change replacement of line 114 is: return super.getPrice()*((100 - discount)/100.0f);")
    @Test
    public void testGetDiscountedPriceZeroDiscount() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",0, "10.05.2017", "10.05.2018");

        float value = discountedItem.getDiscountedPrice();

        assertEquals(1.6f, value, 0.001f);
    }

    //TODO: see Ignore
    @Ignore("function getDiscountedPrice() in class DiscountedItem does not divide percents by 100" +
            " so the price result of multiplication is incorrect. " +
            "Possible change replacement of line 114 is: return super.getPrice()*((100 - discount)/100.0f);")
    @Test
    public void testGetDiscountedPriceNozeroDiscount() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");

        float value = discountedItem.getDiscountedPrice();

        assertEquals(0.64f, value, 0.001f);
    }

    @Test
    public void testparseDateCorrectDateFormat() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "abcd", "10.05.2018");

        Date date = discountedItem.getDiscountFrom();

        assertNull(date);
    }

    @Test
    public void testparseDateWrongDateFormat() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");

        Date date = discountedItem.getDiscountFrom();

        assertEquals(10, date.getDate());
        assertEquals(4, date.getMonth());
        assertEquals(117, date.getYear());
    }

    @Test
    public void testSetDiscountToCorrectDateFormatDate() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");
        Date newDate = new Date(120,3,15);

        discountedItem.setDiscountTo(newDate);
        Date date = discountedItem.getDiscountTo();

        assertEquals(15, date.getDate());
        assertEquals(3, date.getMonth());
        assertEquals(120, date.getYear());
    }

    @Test
    public void testSetDiscountToNullDateFormatDate() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");
        Date newDate = null;

        discountedItem.setDiscountTo(newDate);
        Date date = discountedItem.getDiscountTo();

        assertNull(date);
    }


    //TODO: see Ignore
    @Ignore("function getDiscountedPrice() in class DiscountedItem does not divide percents by 100" +
            " so the price result of multiplication is incorrect. " +
            "Possible change replacement of line 114 is: return super.getPrice()*((100 - discount)/100.0f);")
    @Test
    public void testGetDiscountedPriceReflectsChangeOfDiscount() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");
        int newDiscount = 40;

        discountedItem.setDiscount(newDiscount);
        int actual = discountedItem.getDiscount();
        float value = discountedItem.getDiscountedPrice();

        assertEquals(0.96f, value, 0.001f);
        assertEquals(newDiscount, actual);
    }

    @Test
    public void testGetOriginalPricePriceIsCorrect() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");

        float actual = discountedItem.getOriginalPrice();

        assertEquals(1.6f, actual, 0.001f);
    }

    @Test
    public void equalsNotDiscountedItem(){
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");

        assertEquals(false,discountedItem.equals(null));
    }
    //TODO: see Ignore
    @Ignore("function equals(Object object) expeted true, but result is false")

    @Test
    public void equalsDiscountedItem(){
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");
        DiscountedItem discountedItem1 = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");
        assertEquals(true,discountedItem.equals(discountedItem));
    }
}
