package cz.cvut.eshop.storage;

import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ItemStockTest {

    @Test
    public void increaseItemCount() {
        ItemStock itemStock = new ItemStock(null);
        itemStock.IncreaseItemCount(10);

        assertEquals(10, itemStock.getCount());
    }

    @Test
    public void toStringRightString()
    {
        StandardItem item = new StandardItem(1, "Name", 9.68f, "Vegetable", 5);
        int count = 0;
        ItemStock itemStock = new ItemStock(item);
        assertEquals("STOCK OF ITEM:  Item   ID 1   NAME Name   CATEGORY Vegetable   PRICE 9.68   LOYALTY POINTS 5    PIECES IN STORAGE: 0",itemStock.toString());
    }

    @Test
    public void decreaseItemCount() {
        ItemStock itemStock = new ItemStock(null);
        itemStock.decreaseItemCount(10);

        assertEquals(-10, itemStock.getCount());
    }



}