package cz.cvut.eshop.shop;

import org.junit.Test;

import static org.junit.Assert.*;

public class StandardItemTest {

    @Test
    public void copy_copyItem_shouldReturnSameItem() {
        StandardItem item = new StandardItem(1, "Name", 9.68f, "Vegetable", 5);
        StandardItem copyItem = item.copy();
        assertEquals(item, copyItem);
    }

    @Test
    public void ToStringRight()
    {
        StandardItem item = new StandardItem(1, "Name", 9.68f, "Vegetable", 5);
        assertEquals("Item   ID 1   NAME Name   CATEGORY Vegetable   PRICE 9.68   LOYALTY POINTS 5",item.toString());
    }
}