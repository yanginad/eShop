package cz.cvut.eshop.archive;

import cz.cvut.eshop.shop.DiscountedItem;
import cz.cvut.eshop.shop.Item;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class ItemPurchaseArchiveEntryTest extends AbstractArchiveTest {

	//TODO: see Ignore
	@Ignore
	@Test
	public void toStringDoesWork() {
		//return "Item   ID "+id+"   NAME "+name+"   CATEGORY "+category;
		DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");
		Assert.assertEquals("Item   ID 1   NAME name1   CATEGORY category1   ORIGINAL PRICE 1.6    DISCOUNTED PRICE 64.0  DISCOUNT FROM Wed May 10 00:00:00 CEST 2017    DISCOUNT TO Thu May 10 00:00:00 CEST 2018", discountedItem.toString());

	}
	@Test
	public void testGetCountHowManyTimesHasBeenSold() {
		Assert.assertEquals("Incorrect sold count in empty archive", 1, itemArchive.getCountHowManyTimesHasBeenSold());
	}

	@Test
	public void testGetRefItem() {
		Assert.assertEquals("Constructor and getter items are different", item, itemArchive.getRefItem());
	}

	@Test
	public void testIncreaseCountHowManyTimesHasBeenSoldZero() {
		compareIncrement(0);
	}

	@Test
	public void testIncreaseCountHowManyTimesHasBeenSoldPositive() {
		compareIncrement(10);
	}

	@Test
	public void testIncreaseCountHowManyTimesHasBeenSoldNegative() {
		compareIncrement(-10);
	}

	private void compareIncrement(int difference) {
		int soldCount = itemArchive.getCountHowManyTimesHasBeenSold();
		itemArchive.increaseCountHowManyTimesHasBeenSold(difference);
		Assert.assertEquals("Incorrect sold count for " + difference + " increment", soldCount + difference, itemArchive.getCountHowManyTimesHasBeenSold());
	}
}